<?php 
include "conn.php";
include "header.php"; 
?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <!--begin::Form-->
                <form class="form">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card card-custom">
                                <!-- Progress bar holder -->
                                <div id="progress" style="width:1000px;border:10px solid #ccc;"></div>
                                <!-- Progress information -->
                                <div id="information" style="width"></div>
                                <div class="card-header">
                                    <h3 class="card-title">PREVIEW</b></h3>
                                </div>
                                <div class="card-body">
                                    We hereby apply for inclusion on the Approved Companies Database from which companies are selected.
                                    <br><br>
                                    We certify that the information supplied is accurate to the best of our knowledge, and that we accept the conditions and undertakings requested in the Questionnaire.
                                    <br><br>
                                    We understand that false information will result in our exclusion from the CCS Group Plc Approved Companies Database.
                                    <br><br>
                                    We understand that to give or offer any gift or consideration whatsoever as an inducement or reward to any member of the CCS Group Plc with regard to seeking application onto the Approved Companies Database will result in the cancellation of any contracts currently in force and our exclusion from the Approved Companies Database.
                                    <br><br>
                                    We undertake to advise in writing of any material changes provided in this submission.
                                    <br><br>
                                    If hard copy evidence of required documents are not received by the CCS Group plc, your application will not be considered.
                                    <hr>
                                    <b>We have read, understood and accept the terms of the above declaration </b><input type="checkbox">
                                    <br><br>
                                    <input type="hidden" name="sincronizar" value="1">
                                    <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" type="submit">Submit</button>

                                </div>
                                
                            </div>
                        </div>
                    <!--end::Row-->
                    </div>
                    
                </form>
                <!--end::Form-->
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
<?php

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

$datos = datosform();

//var_dump($_SESSION);

// Comprovamos version

try { 
    $soapWsdl = $urlsoap.'VendorPortalAnswers'; 
    //$options = [ 'soap_version' => SOAP_1_1, 'connection_timeout' => 120, 'login' => 'JULIET', 'password' => '89XdU9kLB6UjptBN7xKXHy+d2vSvYjG62AaF6v/HL48=', ]; 
    $client = new SoapClient($soapWsdl, $options); 
    $filter = array();
    $filter[] = array('Field' => 'Web_User', 'Criteria' => '='.$_SESSION["E_Mail"]);
    //$filter[] = array('Field' => 'Answer_type', 'Criteria' => "<>Subtitle");    
    //$filter[] = array('Field' => 'Answer_type', 'Criteria' => "<>Notes");   
    //$filter[] = array('Field' => 'E_Mail', 'Criteria' => '='.strtolower($_POST["inputEmail"]));
    //$filter = ['Field' => 'Customer_Code', 'Criteria' => '='.$_SESSION["customer_code"]]; 
    //$filter = "";
    $result = $client->ReadMultiple(['filter' => $filter, 'setSize' => 1000]);
    //$result = $client->ReadMultiple(['filter' => ['Item_Type' => 'CustCode'], 'setSize' => 1000]); 
    $resposta = $result->ReadMultiple_Result->VendorPortalAnswers;
    //var_dump($filter);
    //var_dump($resposta);
    //die();
    $numero = 0;
    $fecha = date("Y-m-d H:i:s");
    $sqlinsert = "";
    $companyid = $_SESSION['customer_code'];
    foreach($resposta as $datos1){
        if($datos1->Answer_No > $numero){
            //echo "Answer No: ".$datos->Answer_No."<br>";
            $numero = $datos1->Answer_No;
        }
    }
    //echo $numero;
    //die();
    
    //print_r($resposta); 
} catch (Exception $e) { echo $e->getMessage(); }


// Enviar webservice de entrada

if($_REQUEST["sincronizar"] == 1){

    $fechaT = date("Y-m-d")."T".date("H:i:s")."Z";
    // Total processes
    $total  = count($datos);
    //echo $total;
    $i = 1;
    //echo sizeof($datos);
    //die;
    foreach($datos as $key => $value){
        //var_dump($key);
        if($value != ""){
            try { 
                $client = new SoapClient($soapWsdl, $options); 
                $project = ['Answer_No' => $numero+1, 
                            'Question_No' => $key, 
                            'Web_User' => $_SESSION["E_Mail"],
                            'Answer_Text' => $value, 
                            'Answer_Date' => $fechaT];
                $result = $client->Create(['VendorPortalAnswers' => $project]); 
                
                //echo $i;
                
                ?>
                <?php
            } catch (Exception $e) { 
                ?>
                <div class="alert alert-danger" role="alert">
                    <?php //print_r($project); ?>
                    <?php echo $e->getMessage(); ?>
                </div>
                <?php 
            }
            
        }
        //print_r($i); 
        // Calculate the percentation
        $percent = intval($i/$total * 100)."%";
            
        // Javascript for updating the progress bar and information
        echo '<script language="javascript">
        document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
        document.getElementById("information").innerHTML="'.$i.' row(s) processed.";
        </script>';
        // This is for the buffer achieve the minimum size in order to flush data
        echo str_repeat(' ',1024*64);
        // Send output to browser immediately
        flush();
        // Sleep one second so we can see the delay
        //sleep(1);
        $i++;
    }
    // Tell user that the process is completed
    echo '<script language="javascript">document.getElementById("information").innerHTML="Process completed"</script>';
}
?>
<?php include "footer.php"; ?>  