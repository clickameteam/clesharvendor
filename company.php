<?php 
include "conn.php";
include "header.php"; 

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

$datos = datosform();
?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <!--begin::Form-->
                <form class="form" method="POST">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">Company Details for  &nbsp;<b>Name Company 1</b></h3>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <!--
                                    <div class="form-group">
                                        <div class="alert alert-custom alert-default" role="alert">
                                            <div class="alert-icon"><i class="flaticon-warning text-primary"></i></div>
                                            <div class="alert-text">Info</div>
                                        </div>
                                    </div>
                                    -->
                                    <div class="form-group">
                                        <label>Company</label>
                                        <input type="text" class="form-control" placeholder="Name of company" id="field20" name ="field20" value="<?php echo $datos[20];?>"/>
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Type of company</label>
                                        <select id="field30" name ="field30" class="form-control">
                                            <option seleted></option>
                                            <option <?php if($datos[30] == 'Limited (Private)') echo "selected";?>>Limited (Private)</option>
                                            <option <?php if($datos[30] == 'Limited (Public)') echo "selected";?>>Limited (Public)</option>
                                            <option <?php if($datos[30] == 'Partnership') echo "selected";?>>Partnership</option>
                                            <option <?php if($datos[30] == 'Public (Unlimited)') echo "selected";?>>Public (Unlimited)</option>
                                            <option <?php if($datos[30] == 'Sole Founder') echo "selected";?>>Sole Founder</option>
                                            <option <?php if($datos[30] == 'Sole Trader') echo "selected";?>>Sole Trader</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea class="form-control" id="field40" name ="field40"><?php echo $datos[40];?></textarea>
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Postcode</label>
                                        <input type="text" class="form-control" placeholder="Postcode" id="field70" name ="field70"  value="<?php echo $datos[50];?>">
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Website</label>
                                        <input type="text" class="form-control" placeholder="URL" id="field80" name ="field80" value="<?php echo $datos[80];?>">
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Telephone</label>
                                        <input type="text" class="form-control" placeholder="Phone number" id="field90" name ="field90" value="<?php echo $datos[90];?>">
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Fax</label>
                                        <input type="text" class="form-control" placeholder="Fax number" id="field100" name ="field100" value="<?php echo $datos[100];?>" >
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    
                                    
                                    
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Contact Name</label>
                                        <input type="text" class="form-control" placeholder="Name" id="field110" name ="field110" value="<?php echo $datos[110];?>">
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Telephone</label>
                                        <input type="text" class="form-control" placeholder="Contact telephone" id="field120" name ="field120" value="<?php echo $datos[120];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Fax</label>
                                        <input type="text" class="form-control" placeholder="Contact Fax" id="field130" name ="field130"  value="<?php echo $datos[130];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Email 1</label>
                                        <input type="text" class="form-control" placeholder="Contact Email" id="field140" name ="field140" value="<?php echo $datos[140];?>"/>
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Email 2</label>
                                        <input type="text" class="form-control" placeholder="Contact Email" id="field150" name ="field150" value="<?php echo $datos[150];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="separator separator-dashed my-8"></div>
                                    <b>Registered Office</b>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea class="form-control" id="field170" name ="field170"><?php echo $datos[170];?></textarea>
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Postcode</label>
                                        <input type="text" class="form-control" placeholder="Postcode" id="field200" name ="field200"  value="<?php echo $datos[200];?>"/>
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">General Information</h3>
                                </div>
                                <div class="card-body">
                                    <b>Preferred geographical working area</b><br><br>
                                    <div class="col-8" style="float:left; font-weight: bold; border-bottom: 1px solid #ebedf3;">Area</div>
                                    <div class="col-4" style="float:left; font-weight: bold; border-bottom: 1px solid #ebedf3; text-align: center;">Selected</div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Central London</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field240" name="field240" value="1" <?php if($datos[240] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Greater London</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field250" name="field250" value="1" <?php if($datos[250] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Inside M25 (North)	</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field260" name="field260" value="1" <?php if($datos[260] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Inside M25 (South)</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field270" name="field270" value="1" <?php if($datos[270] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Inside M25 (East)</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field280" name="field280" value="1" <?php if($datos[280] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Inside M25 (West)</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field290" name="field290" value="1" <?php if($datos[290] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Anywhere inside the M25</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field300" name="field300" value="1" <?php if($datos[300] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Upto 50 miles outside the M25</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field310" name="field310" value="1" <?php if($datos[310] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">All locations</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field320" name="field320" value="1" <?php if($datos[320] == 1) echo "checked";?>>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <b>Do you undertake work on any of the following rail networks?</b><br><br>
                                    <div class="col-8" style="float:left; font-weight: bold; border-bottom: 1px solid #ebedf3;">Rail Network	</div>
                                    <div class="col-4" style="float:left; font-weight: bold; border-bottom: 1px solid #ebedf3; text-align: center;">Selected</div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">London Underground	</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field350" name="field350" value="1" <?php if($datos[350] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Network Rail	</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field360" name="field360" value="1" <?php if($datos[360] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">National Rail		</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field370" name="field370" value="1" <?php if($datos[370] == 1) echo "checked";?>>
                                    </div>
                                    <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">None of the above	</div>
                                    <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                        <input type="checkbox" id="field380" name="field380" value="1" <?php if($datos[380] == 1) echo "checked";?>>
                                    </div>
                                    
                                </div>
                                <div class="card-header">
                                    <h3 class="card-title">Additional Information</h3>
                                </div>
                                <div class="card-body">
                                    <b>Please include any additional information that you feel is relevant to your submission 
                                    that will help us in our evaluation of your company.</b>
                                    <div class="form-group">
                                        <textarea class="form-control" id="field410" name ="field410" ><?php echo $datos[410];?></textarea>
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                </div>
                            </div>   
                        </div>
                    <!--end::Row-->
                    </div>
                  
                    <div class="row">
                        <div class="col-7">
                        </div>
                        <div class="col-5" style="text-align: right;">
                            <button class="btn btn-secondary font-weight-bold px-9 py-4 my-3 mx-2">Back</button>
                            <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, false)" type="button">Save</button>
                            <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, true)" type="button">Save and continue</button>
                        </div>
                    </div>
                </form>                  
                <!--end::Form-->
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

<?php include "footer.php"; ?>   
<script>
    async function saveForm(element, exit){
        let payload = {};

        let form = element.closest('form');

        let inputs = form.querySelectorAll('input[type="text"]');
        inputs.forEach(element => payload[element.name] = element.value);

        let dates = form.querySelectorAll('input[type="date"]');
        dates.forEach(element => payload[element.name] = element.value);            

        let checkboxes = form.querySelectorAll('input[type="checkbox"]');
        checkboxes.forEach(element => payload[element.name] = element.checked ? 1 : 0);
       
        let selects = form.querySelectorAll('select');
        selects.forEach(element => payload[element.name] = element.value);
        
        let textareas = form.querySelectorAll('textarea');
        textareas.forEach(element => payload[element.name] = element.value);
        console.log(exit);
        await fetch('saveform.php', {
            method:"POST",
            body:JSON.stringify(payload)
        })
        .then(response => response)
        .then(response => {
            if(exit){
                location.href = "financial.php";
            }
            
        })
        .catch(response => {
            //Hacer otra cosa
        });
        
    }
    </script>                 
