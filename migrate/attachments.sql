-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 08-03-2021 a las 07:13:09
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cleshar_vendor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `attachments`
--

DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `id_field` int(10) NOT NULL,
  `fieldvalue` text COLLATE utf8_spanish_ci NOT NULL,
  `data_insert` datetime NOT NULL,
  `data_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `attachments`
--

INSERT INTO `attachments` (`id`, `company_id`, `id_field`, `fieldvalue`, `data_insert`, `data_update`) VALUES
(1, '1STCA001', 530, 'upload/530_1STCA001_20210307173337.pdf', '2021-03-07 17:33:37', '2021-03-07 17:33:37'),
(2, '1STCA001', 530, 'upload/530_1STCA001_20210307173437.pdf', '2021-03-07 17:34:37', '2021-03-07 17:34:37'),
(3, '1STCA001', 530, 'upload/530_1STCA001_20210307175704.pdf', '2021-03-07 17:57:04', '2021-03-07 17:57:04');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
