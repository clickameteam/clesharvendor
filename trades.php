<?php 
include "conn.php";
include "header.php"; 

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

$datos = datosform();

?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <!--begin::Form-->
                <form class="form">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">Trades infromation for  &nbsp;<b>Name Company 1</b></h3>                                
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <b>Please select the trading areas that your company operates in</b>
                                    <div class="alert alert-custom alert-default" role="alert">
                                        <div class="alert-text">At least one trading area is needed, multiple selections acceptable.</div>
                                    </div>
                                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                        <div class="alert-text">After selecting your trading areas you must press the Save button below. You can then select your primary trade in the drop down list to the right.</div>
                                    </div>                                    
                                </div>
                                <div class="card-body" style="padding: 5px 30px 10px;"> 
                                    <div style="overflow: auto; max-height: 35vh">
                                        <div class="col-8" style="float:left; font-weight: bold; border-bottom: 1px solid #ebedf3;">Trade</div>
                                        <div class="col-4" style="float:left; font-weight: bold; border-bottom: 1px solid #ebedf3; text-align: center;">Selected</div>
                                        
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Architectural metalwork</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1770" name="field1770" value="1" <?php if($datos[1770] == 1) echo "checked";?>>
                                        </div>
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Architectural services</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1780" name="field1780" value="1" <?php if($datos[1780] == 1) echo "checked";?>>
                                        </div>
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Brick/Block work</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1790" name="field1790" value="1" <?php if($datos[1790] == 1) echo "checked";?>>
                                        </div>
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Calibration of equipment</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1800" name="field1800" value="1" <?php if($datos[1800] == 1) echo "checked";?>>
                                        </div>
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Carpentry 1st and 2nd fix</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1810" name="field1810" value="1" <?php if($datos[1810] == 1) echo "checked";?>>
                                        </div>
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">CCTV underground drainage survey</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1820" name="field1820" value="1" <?php if($datos[1820] == 1) echo "checked";?>>
                                        </div>
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Civil engineering</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1830" name="field1830" value="1" <?php if($datos[1830] == 1) echo "checked";?>>
                                        </div>
                                        <div class="col-8" style="float:left; border-bottom: 1px solid #ebedf3; height: 30px; padding-top: 5px;">Cleaning</div>
                                        <div class="col-4" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 30px; padding-top: 5px;">
                                            <input type="checkbox" id="field1840" name="field1840" value="1" <?php if($datos[1840] == 1) echo "checked";?>>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">PRIMARY TRADE</b></h3>
                                </div>
                                <div class="card-body">
                                        <b>Please select the primary trade from the list of previously selected trades</b>
                                        <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                            <div class="alert-text">If no trades are showing in the list below, please select at least one trade from the panel on the left and press the update button.</div>
                                        </div>
                                        <div class="form-group">
                                            <label>Primary Trade</label>
                                            <select id="field2710" name ="field2710" class="form-control">
                                                <option value="0" <?php if($datos[2710] == 0) echo "selected";?>>---</option>
                                                <option value="1" <?php if($datos[2710] == 1) echo "selected";?>>Architectural metalwork</option>
                                                <option value="2" <?php if($datos[2710] == 2) echo "selected";?>>Architectural services </option>
                                            </select>
                                        </div>        
                                    </div>
                                </div>                                                 
                            </div>
                        
                        
                    <!--end::Row-->
                    </div>
                    <div class="row">
                        <div class="col-7">
                        </div>
                        <div class="col-5" style="text-align: right;">
                            <a href="hsqe2.php"><button class="btn btn-secondary font-weight-bold px-9 py-4 my-3 mx-2">Back</button></a>
                            <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, false)" type="button">Save</button>
                            <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, true)" type="button">Save and continue</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
    
<?php include "footer.php"; ?>                    
<script>
    async function saveForm(element, exit){
        let payload = {};

        let form = element.closest('form');

        let inputs = form.querySelectorAll('input[type="text"]');
        inputs.forEach(element => payload[element.name] = element.value);

        let dates = form.querySelectorAll('input[type="date"]');
        dates.forEach(element => payload[element.name] = element.value);        

        let checkboxes = form.querySelectorAll('input[type="checkbox"]');
        checkboxes.forEach(element => payload[element.name] = element.checked ? 1 : 0);
       
        let selects = form.querySelectorAll('select');
        selects.forEach(element => payload[element.name] = element.value);
        
        let textareas = form.querySelectorAll('textarea');
        textareas.forEach(element => payload[element.name] = element.value);
        console.log(exit);
        await fetch('saveform.php', {
            method:"POST",
            body:JSON.stringify(payload)
        })
        .then(response => response)
        .then(response => {
            if(exit){
                location.href = "preview.php";
            }
            
        })
        .catch(response => {
            //Hacer otra cosa
        });
        
    }
    const abrir = element => {
        let id = element.id;
        let value = element.value;
        let options = $("#" + id + " option");
        let nOptions = options.length;
        for(i = 0; i < nOptions; i++){
            $(".grupo" + i).addClass("d-none");
        }
        $(".grupo" + value).removeClass("d-none");
        console.log(value);
    }
    </script>   