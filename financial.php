<?php
include "conn.php";
include "header.php";

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

$datos = datosform();


?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <!--begin::Form-->
            <form class="form">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card card-custom">
                            <div class="card-header">
                                <h3 class="card-title">Financial infromation for &nbsp;<b>Name Company 1</b></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card card-custom">
                            <div class="card-body">
                                <b>Company Registration</b>
                                <div class="form-group">
                                    <label>Date of Registration</label>
                                    <input type="date" class="form-control" id="field430" name="field430" value="<?php echo $datos[430]; ?>" />
                                    <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                </div>
                                <div class="form-group">
                                    <label>Registration No.</label>
                                    <input type="text" class="form-control" placeholder="Registration No." id="field440" name="field440" value="<?php echo $datos[440]; ?>" />
                                    <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                </div>
                                <div class="form-group">
                                    <label>UTR No.</label>
                                    <input type="text" class="form-control" placeholder="UTR No." id="field450" name="field450" value="<?php echo $datos[450]; ?>" />
                                    <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                </div>
                                <div class="form-group">
                                    <label>VAT No.</label>
                                    <input type="text" class="form-control" placeholder="VAT No." id="field460" name="field460" value="<?php echo $datos[460]; ?>" />
                                    <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                </div>
                                <div class="form-group">
                                    <label>Associated Companies </label>
                                    <select id="field470" name="field470" class="form-control">
                                        <option <?php if ($datos[470] == 'N') echo "selected"; ?>>N</option>
                                        <option <?php if ($datos[470] == 'Y') echo "selected"; ?>>Y</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>If Yes, give details </label>
                                    <textarea class="form-control" id="field480" name="field480" /><?php echo $datos[480]; ?></textarea>
                                    <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                </div>
                                <div class="form-group">
                                    <label>Parent Company</label>
                                    <select id="field490" name="field490" class="form-control">
                                        <option <?php if ($datos[490] == 'N') echo "selected"; ?>>N</option>
                                        <option <?php if ($datos[490] == 'Y') echo "selected"; ?>>Y</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>If Yes, give details </label>
                                    <textarea class="form-control" id="field500" name="field500" /><?php echo $datos[500]; ?></textarea>
                                    <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                </div>
                                <div class="form-group">
                                    <label>Contract guarantee by Parent Company</label>
                                    <select id="field510" name="field510" class="form-control">
                                        <option <?php if ($datos[510] == 'N') echo "selected"; ?>>N</option>
                                        <option <?php if ($datos[510] == 'Y') echo "selected"; ?>>Y</option>
                                    </select>
                                </div>




                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card card-custom">
                            <div class="card-body">
                                <b>Annual Turnover</b>
                                <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                    <div class="alert-text">Hard copy evidence required</div>
                                </div>
                                <div class="dropzone dropzone-default dropzone-primary dz-clickable" id="myDropzone">
                                    <div class="dropzone-msg dz-message needsclick">
                                        <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                        <span class="dropzone-msg-desc">Upload up to 10 files</span>
                                    </div>
                                </div>
                                <div class="archivosSubidos row" id="archivosSubidos">

                                </div>
                                <div class="w-100" style="clear:both"> </div>
                                <br/>
                                <br/>
                                <div class="form-group">
                                    <label>2017-2018</label>
                                    <select id="field540" name="field540" class="form-control">
                                        <option value="0" <?php if ($datos[540] == 0) echo "selected"; ?>>Up to £50 000</option>
                                        <option value="1" <?php if ($datos[540] == 1) echo "selected"; ?>>£50 000 - £100 000</option>
                                        <option value="2" <?php if ($datos[540] == 2) echo "selected"; ?>>£100 000 - £200 000</option>
                                        <option value="3" <?php if ($datos[540] == 3) echo "selected"; ?>>£200 000 - £300 000</option>
                                        <option value="5" <?php if ($datos[540] == 4) echo "selected"; ?>>£300 000 - £400 000</option>
                                        <option value="6" <?php if ($datos[540] == 5) echo "selected"; ?>>£400 000 - £500 000</option>
                                        <option value="7" <?php if ($datos[540] == 6) echo "selected"; ?>>£500 000 - £750 000</option>
                                        <option value="8" <?php if ($datos[540] == 7) echo "selected"; ?>>£750 000 - £1 000 000</option>
                                        <option value="9" <?php if ($datos[540] == 8) echo "selected"; ?>>£1M - £2M</option>
                                        <option value="10" <?php if ($datos[540] == 9) echo "selected"; ?>>£2M - £3M</option>
                                        <option value="11" <?php if ($datos[540] == 10) echo "selected"; ?>>£3M - £4M</option>
                                        <option value="12" <?php if ($datos[540] == 11) echo "selected"; ?>>Over £4M</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>2016-2017</label>
                                    <select id="field550" name="field550" class="form-control">
                                        <option value="0" <?php if ($datos[550] == 0) echo "selected"; ?>>Up to £50 000</option>
                                        <option value="1" <?php if ($datos[550] == 1) echo "selected"; ?>>£50 000 - £100 000</option>
                                        <option value="2" <?php if ($datos[550] == 2) echo "selected"; ?>>£100 000 - £200 000</option>
                                        <option value="3" <?php if ($datos[550] == 3) echo "selected"; ?>>£200 000 - £300 000</option>
                                        <option value="5" <?php if ($datos[550] == 4) echo "selected"; ?>>£300 000 - £400 000</option>
                                        <option value="6" <?php if ($datos[550] == 5) echo "selected"; ?>>£400 000 - £500 000</option>
                                        <option value="7" <?php if ($datos[550] == 6) echo "selected"; ?>>£500 000 - £750 000</option>
                                        <option value="8" <?php if ($datos[550] == 7) echo "selected"; ?>>£750 000 - £1 000 000</option>
                                        <option value="9" <?php if ($datos[550] == 8) echo "selected"; ?>>£1M - £2M</option>
                                        <option value="10" <?php if ($datos[550] == 9) echo "selected"; ?>>£2M - £3M</option>
                                        <option value="11" <?php if ($datos[550] == 10) echo "selected"; ?>>£3M - £4M</option>
                                        <option value="12" <?php if ($datos[550] == 11) echo "selected"; ?>>Over £4M</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>2015-2016</label>
                                    <select id="field560" name="field560" class="form-control">
                                        <option value="0" <?php if ($datos[560] == 0) echo "selected"; ?>>Up to £50 000</option>
                                        <option value="1" <?php if ($datos[560] == 1) echo "selected"; ?>>£50 000 - £100 000</option>
                                        <option value="2" <?php if ($datos[560] == 2) echo "selected"; ?>>£100 000 - £200 000</option>
                                        <option value="3" <?php if ($datos[560] == 3) echo "selected"; ?>>£200 000 - £300 000</option>
                                        <option value="5" <?php if ($datos[560] == 4) echo "selected"; ?>>£300 000 - £400 000</option>
                                        <option value="6" <?php if ($datos[560] == 5) echo "selected"; ?>>£400 000 - £500 000</option>
                                        <option value="7" <?php if ($datos[560] == 6) echo "selected"; ?>>£500 000 - £750 000</option>
                                        <option value="8" <?php if ($datos[560] == 7) echo "selected"; ?>>£750 000 - £1 000 000</option>
                                        <option value="9" <?php if ($datos[560] == 8) echo "selected"; ?>>£1M - £2M</option>
                                        <option value="10" <?php if ($datos[560] == 9) echo "selected"; ?>>£2M - £3M</option>
                                        <option value="11" <?php if ($datos[560] == 10) echo "selected"; ?>>£3M - £4M</option>
                                        <option value="12" <?php if ($datos[560] == 11) echo "selected"; ?>>Over £4M</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card card-custom">
                            <div class="card-body">
                                <b>Referees</b>
                                <div class="alert alert-custom alert-light-primary fade show mb-5" role="alert">
                                    <div class="alert-text">Name and Address from whom reference can be sought</div>
                                </div>
                                <div class="form-group">
                                    <label>Bank Reference</label>
                                    <textarea class="form-control" id="field590" name="field590"><?php echo $datos[590]; ?></textarea>
                                    <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                </div>
                                <div class="form-group">
                                    <label>Trade Reference</label>
                                    <textarea class="form-control" id="field600" name="field600"><?php echo $datos[600]; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Row-->
                </div>
                <div class="row">
                    <div class="col-7">
                    </div>
                    <div class="col-5" style="text-align: right;">
                        <a href="company.php"><button class="btn btn-secondary font-weight-bold px-9 py-4 my-3 mx-2">Back</button></a>
                        <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, false)" type="button">Save</button>
                        <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, true)" type="button">Save and continue</button>
                    </div>
                </div>
            </form>
            <!--end::Form-->
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <?php include "footer.php"; ?>
    <script>
        async function saveForm(element, exit) {
            let payload = {};

            let form = element.closest('form');

            let inputs = form.querySelectorAll('input[type="text"]');
            inputs.forEach(element => payload[element.name] = element.value);

            let dates = form.querySelectorAll('input[type="date"]');
            dates.forEach(element => payload[element.name] = element.value);

            let checkboxes = form.querySelectorAll('input[type="checkbox"]');
            checkboxes.forEach(element => payload[element.name] = element.checked ? 1 : 0);

            let selects = form.querySelectorAll('select');
            selects.forEach(element => payload[element.name] = element.value);

            let textareas = form.querySelectorAll('textarea');
            textareas.forEach(element => payload[element.name] = element.value);
            console.log(exit);
            await fetch('saveform.php', {
                    method: "POST",
                    body: JSON.stringify(payload)
                })
                .then(response => response)
                .then(response => {
                    if (exit) {
                        location.href = "insurance.php";
                    }

                })
                .catch(response => {
                    //Hacer otra cosa
                });

        }
    </script>
    <style>
        .preview:hover .previewFile {
            display: none !important;
        }

        .preview:hover .delete {
            display: block;
            cursor: pointer;
        }

        .preview .delete {
            display: none;
        }
    </style>
    <script>
        const getExtension = filename => {
            return filename.split('.').pop();
        }
        const deleteFile = src => {
            swal.fire({
                    html: "You're going to delete this file. <br/>Are you sure?",
                    icon: 'error',
                    showCancelButton: true
                })
                .then(async respuesta => {
                    if (respuesta.isConfirmed) {
                        await fetch('upload.php?question=530&file=' + src, {
                                method: 'DELETE'
                            })
                            .then(response => response.json())
                            .then(response => {
                                swal.fire({
                                    html: response.resultado,
                                    icon: 'info'
                                })
                                retrieveFiles();
                            })
                    }

                });
        }
        const retrieveFiles = _ => {
            $.get('upload.php?question=530', function(data) {
                reloadFiles(data);
            });
        }
        const reloadFiles = data => {
            $("#archivosSubidos").empty();
            if (data.length >= 10){
                $("#myDropzone").addClass('d-none');
            }
            else{
                $("#myDropzone").removeClass('d-none');
            }
            $.each(data, function(key, value) {
                let extension = getExtension(value.name);
                if (extension == 'pdf'){
                    $("#archivosSubidos").append(`
                        <div style='' class='preview col-4 mt-2'>
                            <img src='upload/pdf.jpg' style='width:80px; height:80px;' class='img-fluid previewFile' />
                            <img src='upload/delete.png' style='width:80px; height:80px;' class='img-fluid delete' onclick="deleteFile('${value.name}')" />
                            <a href="${value.name}" target="_BLANK" class="btn btn-outline-primary form-control">Open</a>
                        </div>
                    `);
                    return 0;
                }
                if (extension == 'doc' || extension == 'docx'){
                    $("#archivosSubidos").append(`
                        <div style='' class='preview col-4 mt-2'>
                            <img src='upload/word.png' style='width:80px; height:80px;' class='img-fluid previewFile' />
                            <img src='upload/delete.png' style='width:80px; height:80px;' class='img-fluid delete' onclick="deleteFile('${value.name}')" />
                            <a href="${value.name}" target="_BLANK" class="btn btn-outline-primary form-control">Open</a>
                        </div>
                    `);
                    return 0;
                }
                if (extension == 'xls' || extension == 'xlsx'){
                    $("#archivosSubidos").append(`
                        <div style='' class='preview col-4 mt-2'>
                            <img src='upload/excel.png' style='width:80px; height:80px;' class='img-fluid previewFile' />
                            <img src='upload/delete.png' style='width:80px; height:80px;' class='img-fluid delete' onclick="deleteFile('${value.name}')" />
                            <a href="${value.name}" target="_BLANK" class="btn btn-outline-primary form-control">Open</a>
                        </div>
                    `);
                    return 0;
                }
                $("#archivosSubidos").append(`
                    <div style='' class='preview col-4 mt-2'>
                        <img src='${value.name}' style='width:80px; height:80px;' class='img-fluid previewFile' />
                        <img src='upload/delete.png' style='width:80px; height:80px;' class='img-fluid delete' onclick="deleteFile('${value.name}')" />
                        <a href="${value.name}" target="_BLANK" class="btn btn-outline-primary form-control">Open</a>
                    </div>
                `);

            });

        }
        var myDropzone = new Dropzone("#myDropzone", {
            url: "upload.php?question=530",
            maxFiles: 1,
            parallelUploads: 1,
            acceptedFiles:'.doc,.docx,.xls,.xlsx,.png,.jpg,application/pdf',
            addRemoveLinks: true,
            dictMaxFilesExceeded: "You can only upload upto 10 images",
            init: function() {
                thisDropzone = this;
                this.on("success", function(file, response) {
                    this.removeFile(file);
                    retrieveFiles();
                });
                retrieveFiles();
            }
        });
    </script>