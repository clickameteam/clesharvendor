<?php 
include "conn.php";
include "header.php"; 

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

// Comprobamos los campos que tiene que tener en el webservice
// LLegir PortalUsersListWS
try { 
    $soapWsdl = $urlsoap.'VendorPortalQuestions'; 
    //$options = [ 'soap_version' => SOAP_1_1, 'connection_timeout' => 120, 'login' => 'JULIET', 'password' => '89XdU9kLB6UjptBN7xKXHy+d2vSvYjG62AaF6v/HL48=', ]; 
    $client = new SoapClient($soapWsdl, $options); 
    $filter = array();
    //$filter[] = array('Field' => 'Answer_type', 'Criteria' => "<>Title");
    //$filter[] = array('Field' => 'Answer_type', 'Criteria' => "<>Subtitle");    
    //$filter[] = array('Field' => 'Answer_type', 'Criteria' => "<>Notes");   
    //$filter[] = array('Field' => 'E_Mail', 'Criteria' => '='.strtolower($_POST["inputEmail"]));
    //$filter = ['Field' => 'Customer_Code', 'Criteria' => '='.$_SESSION["customer_code"]]; 
    $filter = "";
    $result = $client->ReadMultiple(['filter' => $filter, 'setSize' => 1000]);
    //$result = $client->ReadMultiple(['filter' => ['Item_Type' => 'CustCode'], 'setSize' => 1000]); 
    $resposta = $result->ReadMultiple_Result->VendorPortalQuestions;
    //var_dump($filter);
    //var_dump($resposta);
    $numero = count($resposta);
    $fecha = date("Y-m-d H:i:s");
    $sqlinsert = "";
    $companyid = $_SESSION['customer_code'];
    foreach($resposta as $datos){
        if($datos->Answer_type != 'Title'){
            if($datos->Answer_type != 'Subtitle'){
                if($datos->Answer_type != 'Notes'){
                    //var_dump($datos->Question_No);
                    //var_dump($datos->Answer_type != 'Title');
                    //var_dump($datos->Answer_type);
                    //echo existefield($companyid, $datos->Question_No);
                    if(existefield($companyid, $datos->Question_No) == 0){
                        $sqlinsert .= "INSERT IGNORE INTO datavalue (company_id, id_field, data_insert, data_update) VALUES ('$companyid', $datos->Question_No, '$fecha', '$fecha');\n";
                    }
                }
            }
        }
    }
    //echo $sqlinsert;
    //die();
    
    //print_r($resposta); 
} catch (Exception $e) { echo $e->getMessage(); }

if($sqlinsert != ""){
    $dbh->beginTransaction();
    $dbh->query($sqlinsert);
    $dbh->commit();
}

//var_dump($datos);

?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card card-custom">
                            <div class="card-header">
                                <h3 class="card-title">START FORM</b></h3>
                            </div>
                            <div class="card-body">
                                <b>Please select start button</b>
                                <a href="company.php">
                                    <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" type="button">START FORM</button>
                                </a>
                                
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card card-custom">
                            <div class="card-header">
                                <h3 class="card-title">SEND FORM TO BUSSINES CENTRAL</b></h3>
                            </div>
                            <div class="card-body">
                                <b>Please select send button</b>
                                <a href="preview.php">
                                    <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" type="button">SEND FORM</button>
                                </a>                                    
                            </div>                                                
                        </div>                       
                    </div>  
                <!--end::Row-->
                </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
<?php include "footer.php"; ?>  