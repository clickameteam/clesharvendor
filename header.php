<?php include("functions.php"); ?>
<?php
session_start();
$_SESSION["customer_code"] = "1STCA001";
if(!$_SESSION["customer_code"]){
    header('Location: login.php');
} else {
	$name = $_SESSION["First_Name"]." ".$_SESSION["Last_Name"];
	//$manager = $_SESSION["manager"];
}
?>
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<meta charset="utf-8" />
		<title>Cleshar - APPROVED COMPANIES REGISTRATION</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendors Styles(used by this page)-->
		<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
		<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.jpg" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js" integrity="sha512-VQQXLthlZQO00P+uEu4mJ4G4OAgqTtKG1hri56kQY1DtdLeIqhKUp9W/lllDDu3uN3SnUNawpW7lBda8+dSi7w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/basic.min.css" integrity="sha512-MeagJSJBgWB9n+Sggsr/vKMRFJWs+OUphiDV7TJiYu+TNQD9RtVJaPDYP8hA/PAjwRnkdvU+NsTncYTKlltgiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed header-bottom-enabled page-loading">
		<!--begin::Main-->
		<!--begin::Header Mobile-->
		<div id="kt_header_mobile" class="header-mobile bg-primary header-mobile-fixed">
			<!--begin::Logo-->
			<a href="index.php">
				<img alt="Logo" src="assets/media/logos/logowhite.png" class="max-h-30px" />
			</a>
			<!--end::Logo-->
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center">
				<button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
					<span></span>
				</button>
				<button class="btn p-0 ml-2" id="kt_header_mobile_topbar_toggle">
					<span class="svg-icon svg-icon-xl">
						<!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<polygon points="0 0 24 0 24 24 0 24" />
								<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
								<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
							</g>
						</svg>
						<!--end::Svg Icon-->
					</span>
				</button>
			</div>
			<!--end::Toolbar-->
		</div>
		<!--end::Header Mobile-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" class="header flex-column header-fixed">
						<!--begin::Top-->
						<div class="header-top" style="background: white!important;">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Left-->
								<div class="d-none d-lg-flex align-items-center mr-3">
									<!--begin::Logo-->
									<a href="index.php" class="mr-20">
										<img alt="Logo" src="assets/media/logos/cleshar.jpg" class="max-h-50px" />
										&nbsp;&nbsp;&nbsp;&nbsp;
										<img alt="Logo" src="assets/media/logos/GPX.jpg" class="max-h-50px" />
										&nbsp;&nbsp;&nbsp;&nbsp;
										<img alt="Logo" src="assets/media/logos/ITS.jpg" class="max-h-50px" />
									</a>
									<!--end::Logo-->
									<!--begin::Tab Navs(for desktop mode)-->
									<?php
									//echo $_SERVER["REQUEST_URI"];
									?>
									<ul class="header-tabs nav align-self-end font-size-lg" role="tablist">
										<!--begin::Item-->
										<li class="nav-item" >
											<a href="/index.php" class="nav-link py-4 px-6 <?php if($_SERVER["REQUEST_URI"] == "/index.php") echo 'active';?>" style="<?php if($_SERVER["REQUEST_URI"] == "/index.php"){ echo 'background: #11228c!important; color: white;';} else { echo 'color: #11228c!important;';}?>" >Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="nav-item" >
											<a href="/company.php" class="nav-link py-4 px-6 <?php if($_SERVER["REQUEST_URI"] == "/company.php") echo 'active';?>" style="<?php if($_SERVER["REQUEST_URI"] == "/company.php"){ echo 'background: #11228c!important; color: white;';} else { echo 'color: #11228c!important;';}?>" >Company Details</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										
										<li class="nav-item mr-3">
											<a href="/financial.php" class="nav-link py-4 px-6 <?php if($_SERVER["REQUEST_URI"] == "/financial.php") echo 'active';?>" style="<?php if($_SERVER["REQUEST_URI"] == "/financial.php"){ echo 'background: #11228c!important; color: white;';} else { echo 'color: #11228c!important;';}?>" >Financial</a>
										</li>
										
										<!--end::Item-->

										<!--begin::Item-->
										
										<li class="nav-item mr-3">
											<a href="/insurance.php" class="nav-link py-4 px-6 <?php if($_SERVER["REQUEST_URI"] == "/insurance.php") echo 'active';?>" style="<?php if($_SERVER["REQUEST_URI"] == "/insurance.php"){ echo 'background: #11228c!important; color: white;';} else { echo 'color: #11228c!important;';}?>" >Insurance</a>
										</li>
										
										<!--end::Item-->

										<!--begin::Item-->
										
										<li class="nav-item mr-3">
											<a href="/hsqe1.php" class="nav-link py-4 px-6 <?php if($_SERVER["REQUEST_URI"] == "/hsqe1.php") echo 'active';?>" style="<?php if($_SERVER["REQUEST_URI"] == "/hsqe1.php"){ echo 'background: #11228c!important; color: white;';} else { echo 'color: #11228c!important;';}?>" >HSQE</a>
										</li>
									
										<!--end::Item-->
										<!--begin::Item-->
										
										<li class="nav-item mr-3">
											<a href="/trades.php" class="nav-link py-4 px-6 <?php if($_SERVER["REQUEST_URI"] == "/trades.php") echo 'active';?>" style="<?php if($_SERVER["REQUEST_URI"] == "/trades.php"){ echo 'background: #11228c!important; color: white;';} else { echo 'color: #11228c!important;';}?>" >Trades</a>
										</li>
										
										<!--end::Item-->
										<!--begin::Item-->
										
										<li class="nav-item mr-3">
											<a href="/preview.php" class="nav-link py-4 px-6 <?php if($_SERVER["REQUEST_URI"] == "/preview.php") echo 'active';?>" style="<?php if($_SERVER["REQUEST_URI"] == "/preview.php"){ echo 'background: #11228c!important; color: white;';} else { echo 'color: #11228c!important;';}?>" >Preview</a>
										</li>
										
										<!--end::Item-->
									</ul>
									<!--begin::Tab Navs-->
								</div>
								<!--end::Left-->
								<!--begin::Topbar-->
								<div class="topbar">
									
									<!--begin::User-->
									<div class="topbar-item">
										<!--
										<div class="btn btn-icon btn-primary w-sm-auto d-flex align-items-center btn-lg px-2">
											<div class="d-flex flex-column text-right pr-sm-3">
												<span class="text-white font-weight-bold font-size-sm d-none d-sm-inline">Company Name 1</span>
											</div>
										</div>
										-->
										<a href="logout.php">
										<button id="kt_login_forgot_submit" class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" style="background: red; color: white;">Logout</button>
										</a>
									</div>
									<!--end::User-->
								</div>
								<!--end::Topbar-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Top-->
						<!--begin::Bottom-->
						<div class="header-bottom" style="background: #11228c!important;">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Header Menu Wrapper-->
								<div class="header-navs header-navs-left" id="kt_header_navs" style="background: #11228c!important;">
									<!--begin::Tab Navs(for tablet and mobile modes)-->
									<ul class="header-tabs p-5 p-lg-0 d-flex d-lg-none nav nav-bold nav-tabs" role="tablist">
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean active" data-toggle="tab" data-target="#kt_header_tab_1" role="tab">Home</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_2" role="tab">Reports</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_2" role="tab">Orders</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="nav-item mr-2">
											<a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#kt_header_tab_2" role="tab">Help Ceter</a>
										</li>
										<!--end::Item-->
									</ul>
									<!--begin::Tab Navs-->
									
								</div>
								<!--end::Header Menu Wrapper-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Bottom-->
					</div>
					<!--end::Header-->