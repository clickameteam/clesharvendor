<?php 
include "conn.php";
include "header.php"; 

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

$datos = datosform();

?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <!--begin::Form-->
                <form class="form">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">HEALTH, SAFETY, QUALITY & ENVIRONMENTAL infromation for  &nbsp;<b>Name Company 1</b></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <div class="alert alert-custom alert-default" role="alert">
                                        <div class="alert-text">Copies of certificates and policies to be sent to scdatabase@ccs-group.co.uk</div>
                                    </div>
                                </div>
                                <div class="card-body" style="padding: 5px 30px 10px;"> 
                                    <b>12. Please provide details of any HSE/ORR Prohibition/Improvement Notices, Environmental Agency Enforcement Notices or Prosecutions in the last 3 years.</b>
                                    <textarea class="form-control" id="field1460" name ="field1460"><?php echo $datos[1460];?></textarea>
                                </div>  
                                <div class="card-body" style="padding: 5px 30px 10px;"> 
                                    <b>13. What lessons have you learned from recent accidents, environmental incidents or non-conformances in the last year?</b>
                                    <textarea class="form-control" id="field1490" name ="field1490"><?php echo $datos[1490];?></textarea>
                                </div>  
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>14. Do you subcontract part of your works?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1500" name ="field1500" class="form-control">
                                            <option value="Y" <?php if($datos[1500] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1500] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div> 
                                    <textarea class="form-control" id="field1510" name ="field1510"><?php echo $datos[1510];?></textarea>     
                                </div> 
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>15. Do you risk assess the activities you carry out?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1520" name ="field1520" class="form-control">
                                            <option value="Y" <?php if($datos[1520] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1520] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                </div> 
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>16. Do you prepare Safe Systems of Work or Method Statements?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1540" name ="field1540" class="form-control">
                                            <option value="Y" <?php if($datos[1540] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1540] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                </div> 
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>17. Do these include for health risks?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1550" name ="field1550" class="form-control">
                                            <option value="Y" <?php if($datos[1550] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1550] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                </div> 
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>18. Do you assess the environmental aspects of your organistaion?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1580" name ="field1580" class="form-control">
                                            <option value="Y" <?php if($datos[1580] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1580] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                </div> 
                                <div class="card-body" style="padding: 5px 30px 10px;"> 
                                    <b>19. How are 15, 16, 17 and 18 communicated to the workfoce?</b>
                                    <textarea class="form-control" id="field1600" name ="field1600"><?php echo $datos[1600];?></textarea>
                                </div> 
                                <div class="card-body" style="padding: 5px 30px 10px;"> 
                                    <b>20. How do you co-ordinate with other contractors on site?</b>
                                    <textarea class="form-control" id="field1610" name ="field1610"><?php echo $datos[1610];?></textarea>
                                </div> 
                            </div>                                                 
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>21. Do you calculate your Carbon Footprint?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1620" name ="field1620" class="form-control">
                                            <option value="Y" <?php if($datos[1620] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1620] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                </div> 
                                <div class="card-body" style="padding: 5px 30px 10px;"> 
                                    <b>22. Details of similar works completed with value and contact.</b>
                                    <textarea class="form-control" id="field1630" name ="field1630"><?php echo $datos[1630];?></textarea>
                                </div> 
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>23. Have you been convicted or had a notice served upon you for infringement of the Modern Slavery Act 2015?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1660" name ="field1660" class="form-control">
                                            <option value="Y" <?php if($datos[1660] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1660] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                </div> 
                                <br><br>
                                <div class="card-body" style="padding: 5px 30px 10px;">                            
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>24(a). Will you be making deliveries?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1690" name ="field1690" class="form-control">
                                            <option value="Y" <?php if($datos[1690] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1690] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;">(b) If yes, do you hold FORS Accreditation?	</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                      <select id="field1700" name ="field1700" class="form-control">
                                            <option value="Y" <?php if($datos[1700] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1700] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>  
                                    (c) If yes, what accreditation do you hold? Please include your FORS ID number.
                                    <textarea class="form-control" id="field1710" name ="field1710"><?php echo $datos[1710];?></textarea>
                                </div> 
                            </div>   
                        </div>
                        
                    <!--end::Row-->
                    </div>
                    <div class="row">
                        <div class="col-7">
                        </div>
                        <div class="col-5" style="text-align: right;">
                            <a href="financial.php"><button class="btn btn-secondary font-weight-bold px-9 py-4 my-3 mx-2">Back</button></a>
                            <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, false)" type="button">Save</button>
                            <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, true)" type="button">Save and continue</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
    
<?php include "footer.php"; ?>                    
<script>
    async function saveForm(element, exit){
        let payload = {};

        let form = element.closest('form');

        let inputs = form.querySelectorAll('input[type="text"]');
        inputs.forEach(element => payload[element.name] = element.value);

        let dates = form.querySelectorAll('input[type="date"]');
        dates.forEach(element => payload[element.name] = element.value);        

        let checkboxes = form.querySelectorAll('input[type="checkbox"]');
        checkboxes.forEach(element => payload[element.name] = element.checked ? 1 : 0);
       
        let selects = form.querySelectorAll('select');
        selects.forEach(element => payload[element.name] = element.value);
        
        let textareas = form.querySelectorAll('textarea');
        textareas.forEach(element => payload[element.name] = element.value);
        console.log(exit);
        await fetch('saveform.php', {
            method:"POST",
            body:JSON.stringify(payload)
        })
        .then(response => response)
        .then(response => {
            if(exit){
                location.href = "trades.php";
            }
            
        })
        .catch(response => {
            //Hacer otra cosa
        });
        
    }
    const abrir = element => {
        let id = element.id;
        let value = element.value;
        let options = $("#" + id + " option");
        let nOptions = options.length;
        for(i = 0; i < nOptions; i++){
            $(".grupo" + i).addClass("d-none");
        }
        $(".grupo" + value).removeClass("d-none");
        console.log(value);
    }
    </script>   