<?php 
include "conn.php";
include "header.php"; 

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

$datos = datosform();

?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <!--begin::Form-->
                <form class="form">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">HEALTH, SAFETY, QUALITY & ENVIRONMENTAL infromation for  &nbsp;<b>Name Company 1</b></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <div class="alert alert-custom alert-default" role="alert">
                                        <div class="alert-text">Please note that the HSQE questions are split onto two pages and the number of questions you are required to answer depends on how many employees you have. If you have less than 21 employees some questions will be missing and if you are a sole trader there are no questions to answer on the second page.</div>
                                    </div>
                                    <br><br>
                                    <div class="form-group">
                                        <label>How many employees do you have?:</label>
                                        <select id="field900" name ="field900" class="form-control" onchange="abrir(this)" onload="abrir(this)">
                                            <option value="0" <?php if($datos[900] == 0) echo "selected";?>>--</option>
                                            <option value="1" <?php if($datos[900] == 1) echo "selected";?>>0</option>
                                            <option value="2" <?php if($datos[900] == 2) echo "selected";?>>1-5</option>
                                            <option value="3" <?php if($datos[900] == 3) echo "selected";?>>6-20</option>
                                            <option value="4" <?php if($datos[900] == 4) echo "selected";?>>21+</option>
                                        </select>
                                    </div>  
                                    
                                </div>
                                <div class="card-body grupo1 grupo2 grupo3 grupo4 d-none">
                                    <b>1. Do you have the following signed and published?</b>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 50px; padding: 15px 2px 0 2px;">H&S Policy		</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 50px; padding: 5px 2px 0 2px;
">
                                        <select id="field920" name ="field920" class="form-control">
                                            <option value="Y" <?php if($datos[920] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[920] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 50px; padding: 15px 2px 0 2px;">Quality Policy		</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 50px; padding: 5px 2px 0 2px;">
                                        <select id="field930" name ="field930" class="form-control">
                                            <option value="Y" <?php if($datos[930] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[930] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>                                    
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 50px; padding: 15px 2px 0 2px;">Environmental Policy		</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 50px; padding: 5px 2px 0 2px;">
                                        <select id="field940" name ="field940" class="form-control">
                                            <option value="Y" <?php if($datos[940] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[940] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">Anti-Trafficking / Modern Slavery Policy	</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 60px; padding: 5px 2px 0 2px;">
                                        <select id="field950" name ="field950" class="form-control">
                                            <option value="Y" <?php if($datos[950] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[950] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">Anti-Bribery Policy	</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 60px; padding: 5px 2px 0 2px;">
                                        <select id="field960" name ="field960" class="form-control">
                                            <option value="Y" <?php if($datos[960] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[960] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">Equal Opportunities Policy	</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 60px; padding: 5px 2px 0 2px;">
                                        <select id="field970" name ="field970" class="form-control">
                                            <option value="Y" <?php if($datos[970] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[970] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">Are these actively promoted and adhered to?	</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 60px; padding: 5px 2px 0 2px;">
                                        <select id="field980" name ="field980" class="form-control">
                                            <option value="Y" <?php if($datos[980] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[980] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                </div>   
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body grupo0 grupo1 grupo2 grupo3 grupo4">    
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>2.  Do you have a Safety Management system? </b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field990" name ="field990" class="form-control">
                                            <option value="Y" <?php if($datos[990] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[990] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <br><br>
                                    <div class="separator separator-dashed my-8"></div>
                                    <b>Is it certified to ISO 45001:2015 or OHSAS 18001:2007?</b><br>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">
                                        <select id="field1000" name ="field1000" class="form-control">
                                            <option value="Y" <?php if($datos[1000] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1000] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <label style="padding: 10px 0 0 20px;">Expiry</label><input type="date" class="form-control" id="field1010" name ="field1010" value="<?php echo $datos[1010];?>" style="float: right; width: 180px!important" />
                                    </div> 
                                    <b>Is it certified to BS EN ISO 9001:2015?</b><br>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">
                                        <select id="field1020" name ="field1020" class="form-control">
                                            <option value="Y" <?php if($datos[1020] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1020] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <label style="padding: 10px 0 0 20px;">Expiry</label><input type="date" class="form-control" id="field1030" name ="field1030" value="<?php echo $datos[1030];?>" style="float: right; width: 180px!important" />
                                    </div> 
                                    <b>Is it certified to BS EN ISO 14001:2015?</b><br>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">
                                        <select id="field1040" name ="field1040" class="form-control">
                                            <option value="Y" <?php if($datos[1040] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1040] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <label style="padding: 10px 0 0 20px;">Expiry</label><input type="date" class="form-control" id="field1050" name ="field1050" value="<?php echo $datos[1050];?>" style="float: right; width: 180px!important" />
                                    </div> 
                                    <b>Is it certified to RISQS?</b><br>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">
                                        <select id="field1060" name ="field1060" class="form-control">
                                            <option value="Y" <?php if($datos[1060] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1060] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <label style="padding: 10px 0 0 20px;">Expiry</label><input type="date" class="form-control" id="field1070" name ="field1070" value="<?php echo $datos[1070];?>" style="float: right; width: 180px!important" />
                                    </div>  
                                </div> 
                                <div class="card-body grupo0 grupo1 grupo2 grupo3 grupo4"> 
                                    <div class="col-12" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>3. Are you registered with any of the following trade bodies? </b>		</div>
                                    
                                    <br><br>
                                    <div class="separator separator-dashed my-8"></div>
                                    <b>NJCBI</b><br>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">
                                        <select id="field1090" name ="field1090" class="form-control">
                                            <option value="Y" <?php if($datos[1090] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1090] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <label style="padding: 10px 0 0 20px;">Number</label><input type="text" class="form-control" id="field1100" name ="field1100" value="<?php echo $datos[1100];?>" style="float: right; width: 180px!important" />
                                    </div> 
                                    <b>CITB</b><br>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">
                                        <select id="field1110" name ="field1110" class="form-control">
                                            <option value="Y" <?php if($datos[1110] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1110] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <label style="padding: 10px 0 0 20px;">Number</label><input type="text" class="form-control" id="field1120" name ="field1120" value="<?php echo $datos[1120];?>" style="float: right; width: 180px!important" />
                                    </div> 
                                    <b>NHBC</b><br>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; height: 60px; padding: 15px 2px 0 2px;">
                                        <select id="field1130" name ="field1130" class="form-control">
                                            <option value="Y" <?php if($datos[1130] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1130] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <label style="padding: 10px 0 0 20px;">Number</label><input type="text" class="form-control" id="field1140" name ="field1140" value="<?php echo $datos[1140];?>" style="float: right; width: 180px!important" />
                                    </div> <br><br><br><br>
                                    <b>Other memberships (e.g. CHSG)</b><br>
                                        <textarea class="form-control" id="field1150" name ="field1150"><?php echo $datos[1150];?></textarea>
                                </div> 
                                <div class="card-body grupo2 grupo3 grupo4 d-none">          
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>4. Do you have access to competent safety advice?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1170" name ="field1170" class="form-control">
                                            <option value="Y" <?php if($datos[1170] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1170] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>
                                    <br><br>
                                    <div class="separator separator-dashed my-8"></div>
                                    <b>Who from?</b><br>
                                    
                                    <div class="col-12" style="float:left; border-bottom: 1px solid #ebedf3; text-align: left; height: 60px; padding: 15px 2px 0 2px;">
                                        <input type="text" class="form-control" id="field1190" name ="field1190" value="<?php echo $datos[1190];?>"/>
                                    </div> 
                                </div>   
                            </div>                                                 
                        </div>
                        <div class="col-xl-4">
                            <div class="card card-custom">
                                <div class="card-body grupo1 grupo2 grupo3 grupo4 d-none">      
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>5. Do you comply with all Health, Safety and Environmental legislation?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1210" name ="field1210" class="form-control">
                                            <option value="Y" <?php if($datos[1210] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1210] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>    
                                </div>   
                                <div class="card-body grupo3 grupo4 d-none"> 
                                    <b>6. What Health, Safety and Environmental training do you provide your workforce, do you provide Inductions?</b>
                                    <textarea class="form-control" id="field1220" name ="field1220"><?php echo $datos[1220];?></textarea>
                                </div>   
                                <div class="card-body grupo3 grupo4 d-none">   
                                    <b>7. What activity appropriate training do you provide your workforce?</b>
                                    <textarea class="form-control" id="field1230" name ="field1230"><?php echo $datos[1230];?></textarea>
                                </div>  
                                <div class="card-body grupo1 grupo2 grupo3 grupo4 d-none"> 
                                    <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>8. Do you ensure all operatives are legitimately able to work in the UK?</b>		</div>
                                    <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1240" name ="field1240" class="form-control">
                                            <option value="Y" <?php if($datos[1240] == "Y") echo "selected";?>>Y</option>
                                            <option value="N" <?php if($datos[1240] == "N") echo "selected";?>>N</option>
                                        </select>
                                    </div>    
                                </div>   
                                <div class="card-body">                                   
                                    <div class="grupo0 grupo1 grupo2 grupo3 grupo4">
                                        <div class="col-9" style="float:left; height: 60px; padding: 15px 2px 0 2px;"><b>9. Do you carry out audits or inspections to ensure your systems and legislation are followed on site?</b>		</div>
                                        <div class="col-3" style="float:left; text-align: center; height: 60px; padding: 10px 2px 0 2px;
">                                            <select id="field1270" name ="field1270" class="form-control">
                                                <option value="Y" <?php if($datos[1270] == "Y") echo "selected";?>>Y</option>
                                                <option value="N" <?php if($datos[1270] == "N") echo "selected";?>>N</option>
                                            </select>
                                        </div>                                        
                                    </div>   
                                </div>
                                <div class="card-body grupo3 grupo4 d-none">   
                                    <b>10. How do you consult with your workforce on Health and Safety issues?</b>
                                    <textarea class="form-control" id="field1280" name ="field1280"><?php echo $datos[1280];?></textarea>
                                </div>   
                                <div class="card-body grupo1 grupo2 grupo3 grupo4 d-none">
                                    <b>11. Please provide the following information about accidents involving your workforce for the last year.</b>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 50px; padding: 5px 2px 0 2px;">Number of accidents resulting in loss of 1 shift or more (a)		</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 50px; padding: 5px 2px 0 2px;">
                                        <input type="text" class="form-control" id="field1320" name ="field1320" value="<?php echo $datos[1320];?>" style="float: right; width: 80px!important" />
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 50px; padding: 5px 2px 0 2px;">Number of man hours worked per year (b)		</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 50px; padding: 5px 2px 0 2px;">
                                        <input type="text" class="form-control" id="field1370" name ="field1370" value="<?php echo $datos[1370];?>" style="float: right; width: 80px!important" />
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 50px; padding: 5px 2px 0 2px;">Accident Frequency Rate (a/b x 100 000)		</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 50px; padding: 5px 2px 0 2px;">
                                        <input type="text" class="form-control" id="field1380" name ="field1380" value="<?php echo $datos[1380];?>" style="float: right; width: 80px!important" />
                                    </div>
                                    <div class="col-9" style="float:left; border-bottom: 1px solid #ebedf3; height: 50px; padding: 5px 2px 0 2px;">Number of RIDDOR reportable accidents		</div>
                                    <div class="col-3" style="float:left; border-bottom: 1px solid #ebedf3; text-align: center; height: 50px; padding: 5px 2px 0 2px;">
                                        <input type="text" class="form-control" id="field1410" name ="field1410" value="<?php echo $datos[1410];?>" style="float: right; width: 80px!important" />
                                    </div>
                                </div> 
                            </div>   
                        </div>
                        
                    <!--end::Row-->
                    </div>
                    <div class="row">
                        <div class="col-7">
                        </div>
                        <div class="col-5" style="text-align: right;">
                            <a href="financial.php"><button class="btn btn-secondary font-weight-bold px-9 py-4 my-3 mx-2">Back</button></a>
                            <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, false)" type="button">Save</button>
                            <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, true)" type="button">Save and continue</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
    
<?php include "footer.php"; ?>                    
<script>
    async function saveForm(element, exit){
        let payload = {};

        let form = element.closest('form');

        let inputs = form.querySelectorAll('input[type="text"]');
        inputs.forEach(element => payload[element.name] = element.value);

        let dates = form.querySelectorAll('input[type="date"]');
        dates.forEach(element => payload[element.name] = element.value);        

        let checkboxes = form.querySelectorAll('input[type="checkbox"]');
        checkboxes.forEach(element => payload[element.name] = element.checked ? 1 : 0);
       
        let selects = form.querySelectorAll('select');
        selects.forEach(element => payload[element.name] = element.value);
        
        let textareas = form.querySelectorAll('textarea');
        textareas.forEach(element => payload[element.name] = element.value);
        console.log(exit);
        await fetch('saveform.php', {
            method:"POST",
            body:JSON.stringify(payload)
        })
        .then(response => response)
        .then(response => {
            if(exit){
                location.href = "hsqe2.php";
            }
            
        })
        .catch(response => {
            //Hacer otra cosa
        });
        
    }
    const abrir = element => {
        let id = element.id;
        let value = element.value;
        let options = $("#" + id + " option");
        let nOptions = options.length;
        for(i = 0; i < nOptions; i++){
            $(".grupo" + i).addClass("d-none");
        }
        $(".grupo" + value).removeClass("d-none");
        console.log(value);
    }
    </script>   