<?php 
include "conn.php";
include "header.php"; 

// Comprovamos si existen los campos y ejecutamos el insert de todas las preguntas:

$datos = datosform();

?>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <!--begin::Form-->
                <form class="form">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <h3 class="card-title">Insurance infromation for  &nbsp;<b>Name Company 1</b></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="card card-custom">
                                <div class="card-body">
                                <b>Employers Liability Insurance</b>
                                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                        <div class="alert-text">Hard copy evidence required</div>
                                    </div>
                                    <div class="dropzone dropzone-default dropzone-primary dz-clickable" id="kt_dropzone_2">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                            <span class="dropzone-msg-desc">Upload up to 10 files</span>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="form-group">
                                        <label>Insurer</label>
                                        <input type="text" class="form-control" placeholder="Insurer" id="field630" name ="field630" value="<?php echo $datos[630];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Policy Number</label>
                                        <input type="text" class="form-control" placeholder="Policy Number" id="field640" name ="field640" value="<?php echo $datos[640];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Limit of Cover (£)</label>
                                        <input type="text" class="form-control" placeholder="Limit of Cover (£)" id="field650" name ="field650" value="<?php echo $datos[650];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Insurance Expiry Date</label>
                                        <input type="date" class="form-control" id="field660" name ="field660" value="<?php echo $datos[660];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>                             
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3">
                            <div class="card card-custom">
                                <div class="card-body">
                                <b>Public Liability Insurance</b>
                                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                        <div class="alert-text">Hard copy evidence required</div>
                                    </div>
                                    <div class="dropzone dropzone-default dropzone-primary dz-clickable" id="kt_dropzone_2">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                            <span class="dropzone-msg-desc">Upload up to 10 files</span>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="form-group">
                                        <label>Insurer</label>
                                        <input type="text" class="form-control" placeholder="Insurer" id="field690" name ="field690" value="<?php echo $datos[690];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Policy Number</label>
                                        <input type="text" class="form-control" placeholder="Policy Number" id="field700" name ="field700" value="<?php echo $datos[700];?>"  />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Limit of Cover (£)</label>
                                        <input type="text" class="form-control" placeholder="Limit of Cover (£)" id="field710" name ="field710" value="<?php echo $datos[710];?>"  />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Insurance Expiry Date</label>
                                        <input type="date" class="form-control" id="field720" name ="field720" value="<?php echo $datos[720];?>"  />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                </div>
                            </div>                    
                        </div>
                        <div class="col-xl-3">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <b>Professional Indemnity Insurance</b>
                                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                        <div class="alert-text">Hard copy evidence required</div>
                                    </div>
                                    <div class="dropzone dropzone-default dropzone-primary dz-clickable" id="kt_dropzone_2">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                            <span class="dropzone-msg-desc">Upload up to 10 files</span>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="form-group">
                                        <label>Insurer</label>
                                        <input type="text" class="form-control" placeholder="Insurer" id="field55" name ="field55" value="<?php echo $datos[55];?>"  />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Policy Number</label>
                                        <input type="text" class="form-control" placeholder="Policy Number" id="field56" name ="field56" value="<?php echo $datos[56];?>"  />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Limit of Cover (£)</label>
                                        <input type="text" class="form-control" placeholder="Limit of Cover (£)" id="field57" name ="field57" value="<?php echo $datos[57];?>"  />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Insurance Expiry Date</label>
                                        <input type="date" class="form-control" id="field58" name ="field58" value="<?php echo $datos[58];?>"  />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    
                                </div>
                            </div>   
                        </div>
                        <div class="col-xl-3">
                            <div class="card card-custom">
                                <div class="card-body">
                                    <b>Contractors All Risks Insurance</b>
                                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                        <div class="alert-text">Hard copy evidence required</div>
                                    </div>
                                    <div class="dropzone dropzone-default dropzone-primary dz-clickable" id="kt_dropzone_2">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                            <span class="dropzone-msg-desc">Upload up to 10 files</span>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="form-group">
                                        <label>Insurer</label>
                                        <input type="text" class="form-control" placeholder="Insurer" id="field830" name ="field830" value="<?php echo $datos[830];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Policy Number</label>
                                        <input type="text" class="form-control" placeholder="Policy Number" id="field840" name ="field840" value="<?php echo $datos[840];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Limit of Cover (£)</label>
                                        <input type="text" class="form-control" placeholder="Limit of Cover (£)" id="field850" name ="field850" value="<?php echo $datos[850];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label>Insurance Expiry Date</label>
                                        <input type="date" class="form-control" id="field860" name ="field860" value="<?php echo $datos[860];?>" />
                                        <!--<span class="form-text text-muted">Some help content goes here</span>-->
                                    </div>
                                </div>
                            </div>   
                        </div>
                    <!--end::Row-->
                    </div>
                    <div class="row">
                        <div class="col-7">
                        </div>
                        <div class="col-5" style="text-align: right;">
                            <a href="financial.php"><button class="btn btn-secondary font-weight-bold px-9 py-4 my-3 mx-2">Back</button></a>
                            <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, false)" type="button">Save</button>
                            <button class="btn btn-light-success font-weight-bold px-9 py-4 my-3 mx-2" onclick="saveForm(this, true)" type="button">Save and continue</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
    
<?php include "footer.php"; ?>                    
<script>
    async function saveForm(element, exit){
        let payload = {};

        let form = element.closest('form');

        let inputs = form.querySelectorAll('input[type="text"]');
        inputs.forEach(element => payload[element.name] = element.value);

        let dates = form.querySelectorAll('input[type="date"]');
        dates.forEach(element => payload[element.name] = element.value);        

        let checkboxes = form.querySelectorAll('input[type="checkbox"]');
        checkboxes.forEach(element => payload[element.name] = element.checked ? 1 : 0);
       
        let selects = form.querySelectorAll('select');
        selects.forEach(element => payload[element.name] = element.value);
        
        let textareas = form.querySelectorAll('textarea');
        textareas.forEach(element => payload[element.name] = element.value);
        console.log(exit);
        await fetch('saveform.php', {
            method:"POST",
            body:JSON.stringify(payload)
        })
        .then(response => response)
        .then(response => {
            if(exit){
                location.href = "hsqe1.php";
            }
            
        })
        .catch(response => {
            //Hacer otra cosa
        });
        
    }
    </script>   